def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    return list(range(start,end,step))


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    list = []
    if start <= end:
        while start<end:
            list.append(start)
            start+=step
    else:
        while start>end:
            list.append(start)
            start+=step
    return list

def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    primes = []
    count = 0
    for i in range(10,100):
        for x in range(2,i):
            if(i%x==0):
                break;
        else:
            primes.append(i)
    return primes
    
# print(integers_from_start_to_end_using_while(start = 1, end = 9, step = 1))
# print(integers_from_start_to_end_using_range(1, 10, 1))
# two_digit_primes()