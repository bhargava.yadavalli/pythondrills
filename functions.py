def is_prime(n):
    """
    Check whether a number is prime or not
    """
    for x in range(2,n):
            if n%x==0:
                return False
    else:
        return True


def n_digit_primes(digit=2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    primes = []
    firstnum = 1*(10**(digit-1))
    lastnum = 1*(10**digit)
    for n in range(firstnum,lastnum,1):
        if (n!=1) and is_prime(n):
            primes.append(n)
    return primes