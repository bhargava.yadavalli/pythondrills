"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    with open(path) as file:
        return file.read()


def write_to_file(path, s):
    with open(path,'w') as file:
        file.write(s)


def append_to_file(path, s):
    with open(path,'a') as file:
        file.write(s)


def numbers_and_squares(n, path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    with open(path,'w') as file:
        list = [(x,x*x) for x in range(1,n+1)]
        for x,y in list:
            file.write(str(x)+','+str(y)+'\n')
