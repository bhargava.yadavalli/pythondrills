import re
def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    wordCount = {}
    pattern = re.compile(r'[^\s,.]+')
    words = pattern.findall(s)
    for word in words:
        try:
            wordCount[word] = wordCount[word]+1
        except Exception as e:
            wordCount[word] = 1
    return wordCount
    

def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    return list(d.items())
    


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    return sorted(list(d.items()),key = lambda key:key[0])
    
